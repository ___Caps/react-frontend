import React, { Component } from 'react';
import Nav from './components/Nav';
import LoginForm from './components/LoginForm';
import './App.css';
import {API_URL_BACKEND, API_URL_TOKEN} from "./components/Static";
import {ErrorModal} from "./components/ErrorModal";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displayed_form: '',
      showModal: false,
      logged_in: !!localStorage.getItem('token'),
      user_id: -1,
      username: ''
    };
  }

    openModal = () => this.setState({ showModal: true })

   closeModal = () => this.setState({ showModal: false })


  async componentDidMount() {
    if (this.state.logged_in) {
      await fetch(`${API_URL_BACKEND}/role/`, {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      })
        .then(res => res.json())
        .then(json => {
          this.setState({ role: json.is_superuser });
        });
       await fetch(`${API_URL_BACKEND}/current_user/`, {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      })
        .then(res => res.json())
        .then(json => {
          this.setState({ username: json.username, user_id: json.id });
        });
      
    }
  }

  handle_login = async (e, data) => {
    e.preventDefault();
    await fetch(`${API_URL_TOKEN}/token-auth/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
      .then(res => res.json())
      .then(json => {
            if(json.status !== 200)
            {
              this.openModal()
                console.log('404')
            }
            else {
              localStorage.setItem('token', json.token);
              this.setState({
                logged_in: true,
                displayed_form: '',
                username: json.user.username,
                user_id: json.user.id
              });
            }
      });

      await fetch(`${API_URL_BACKEND}/role/`, {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      })
        .then(res => res.json())
        .then(json => {
          this.setState({ role: json.is_superuser });
        });

  };

  handle_logout = () => {
    localStorage.removeItem('token');
    this.setState({ logged_in: false, username: '' , role: ''});
  };

  display_form = form => {
    this.setState({
      displayed_form: form
    });
  };

  render() {
    let form;
    switch (this.state.displayed_form) {
      case 'login':
        form = <LoginForm handle_login={this.handle_login} />;
        break;
      default:
        form = ''
    }

    return (
      <div className="App">
        <Nav
          logged_in={this.state.logged_in}
          role={this.state.role}
          user_id={this.state.user_id}
          display_form={this.display_form}
          handle_logout={this.handle_logout}
        />
        {form}
            {this.state.showModal && <ErrorModal onOutsideClick={this.closeModal} onEscape={this.closeModal}>
              <h2 style={{color: '#56baed'}}>Login or password didn't match</h2>
              <button  ref={button => button && button.focus()} className="button" onClick={this.closeModal}>
                Close Modal
              </button>
            </ErrorModal>}
      </div>
    );
  }
}

export default App;