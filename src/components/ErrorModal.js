import React from 'react'
import ScrollLock from 'react-scrolllock'
import KeyHandler, { KEYDOWN } from 'react-key-handler'
import './ErrorModal.css'

export const ErrorModal = ({ children, onOutsideClick, onEscape }) => (
  <div className='simple-modal-container' onClick={onOutsideClick}>
    <ScrollLock />
    <KeyHandler
      keyEventName={KEYDOWN}
      keyValue='Escape'
      onKeyHandle={onEscape}
    />
    <div className='simple-modal-inner error' onClick={event => event.stopPropagation()}>
      {children}
    </div>
  </div>
)