import React from 'react';


class SignUpForm extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            username: '',
            email:'',
            avatar:null,
            manager:-1,
            password: '',
          };
          this.onImageChange = this.onImageChange.bind(this);
    }

    onImageChange = async (event) => {
        event.preventDefault();
        await this.setState({
          avatar: event.target.files[0]
        });
    };
  handle_change = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevstate => {
      const newState = { ...prevstate };
      newState[name] = value;
      return newState;
    });
  };
  handle_signup = async (e, data) => {
    e.preventDefault();
    const { history } = this.props;
    let form = new FormData();
    form.append("avatar", this.state.avatar);
    form.append("username", this.state.username);
    form.append("email", this.props.match.params.email);
    form.append("password", this.state.password);
    form.append("manager", this.props.match.params.creator);
    await fetch('http://localhost:8000/backend/register/', {
     method: 'POST',
     headers: {
      //'Content-Type': 'application/json'
    },
     body: form
    }).then(res => res.json())
      .then(json => {
      if(json.status !== 400) {
        localStorage.setItem('token', json.token);
        this.setState({
          logged_in: true,
          username : json.username,
          user_id: json.id
        });

         history.push('/profile/'+json.id);
         window.location.reload(false);
      }
      else {
          console.log('err')
      }
  });
}

  render() {
    return (
        <div className="wrapper">
        <div id="formContent">
          <div className="fadeIn first">
            <h4>Welcome!</h4>
          </div>

          <form onSubmit={e => this.handle_signup(e, this.state)} encType="multipart/form-data">
              <input type="text" id="login" className="fadeIn second" name="username" placeholder="username"
                onChange={this.handle_change} value={this.state.username} required />

              <input style={{marginLeft: 15 + '%'}} type="file" id="avatar" className="fadeIn fourth" name="avatar"  accept="image/png, image/jpeg"
                onChange={this.onImageChange} required />

              <input type="password" id="password" className="fadeIn fourth" name="password" placeholder="password"
                value={this.state.password}  onChange={this.handle_change} required />

              <input type="submit" className="fadeIn fourth" value="Sign Up"/>
          </form>
        </div>
        </div>
    );
  }
}

export default SignUpForm;