import React from 'react';
import PropTypes from 'prop-types';
import './LoginForm.css';

class LoginForm extends React.Component {
  state = {
    username: '',
    password: ''
  };

  handle_change = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevstate => {
      const newState = { ...prevstate };
      newState[name] = value;
      return newState;
    });
  };

  render() {
    return (
        <div className="wrapper">
      <div id="formContent">
          <div className="fadeIn first">

          </div>

          <form onSubmit={e => this.props.handle_login(e, this.state)}>
              <input type="text" id="login" className="fadeIn second" name="username" placeholder="login"
                onChange={this.handle_change} value={this.state.username}/>

              <input type="password" id="password" className="fadeIn third" name="password" placeholder="password"
                value={this.state.password}  onChange={this.handle_change}/>

              <input type="submit" className="fadeIn fourth" value="Log In"/>
          </form>

          <div id="formFooter">
              <a className="underlineHover" href="/">Forgot Password?</a>
          </div>
        </div>
        </div>
    );
  }
}

export default LoginForm;

LoginForm.propTypes = {
  handle_login: PropTypes.func.isRequired
};