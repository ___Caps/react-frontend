import React  from 'react';
import './MyAdvisor.css';
import {API_URL_BACKEND, API_URL_TOKEN} from "./Static";

class MyAdvisor extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      advisor: [],
      user_id: -1,
    };
  }



  componentDidMount() {
      this.refresh_list()
    };

    async refresh_list(){
        await fetch(`${API_URL_BACKEND}/current_user/${this.props.match.params.id}/advisor/`, {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }
        })
          .then(res => res.json())
          .then(data => {
            this.setState({advisor:data })
        })
    }


  render() {
      return (
          <div>
          {this.state.advisor
              ? (
                  <div className="container">
                      <div className="row">
                          <div className="col-md-6 img avatar">
                              <img
                                  src={API_URL_TOKEN+this.state.advisor.avatar}
                                  alt="" className="img-rounded"/>
                          </div>
                          <div className="col-md-4 details">
                              <blockquote>
                                  <h5><b>{this.state.advisor.username}</b></h5>  <br/>
                                  <h5>{this.state.advisor.email}</h5>
                              </blockquote>
                              <h5>{this.state.advisor.message}</h5>
                          </div>
                      </div>
                  </div>
              )
            : (<div className="tbl-center" style={{marginTop: 20+"px"}}>Please waiting .....</div>)
          }
          </div>
      );
  }
}

export default MyAdvisor;