import React  from 'react';
import './MyAdvisor.css';
import {API_URL_BACKEND, API_URL_TOKEN} from "./Static";

class MyProfile extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      user: [],
      user_id: -1,
    };
  }



  componentDidMount() {
      this.refresh_list()
    };

    async refresh_list(){
        await fetch(`${API_URL_BACKEND}/current_user/${this.props.match.params.id}/avatar/`, {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }
        })
          .then(res => res.json())
          .then(data => {
            this.setState({user:data })
        })
    }


  render() {
      return (
          <div>
          {this.state.user
              ? (
                  <div className="container">
                      <div className="row">
                          <div className="col-md-6 img avatar">
                              <img
                                  src={API_URL_TOKEN+this.state.user.avatar}
                                  alt="" className="img-rounded"/>
                          </div>
                          <div className="col-md-5 details">
                              <blockquote>
                                  <h5><b>{this.state.user.username}</b></h5>  <br/>
                                  <h5>{this.state.user.email}</h5>
                              </blockquote>
                          </div>
                      </div>
                  </div>
              )
            : (<div className="tbl-center" style={{marginTop: 20+"px"}}>Please waiting .....</div>)
          }
          </div>
      );
  }
}

export default MyProfile;