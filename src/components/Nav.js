import React from 'react';
import PropTypes from 'prop-types';
import './Nav.css';
import {Link, BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import Profile from './Profile';
import Home from './admin/Home';
import InviteUsers from './admin/InviteUsers';
import TimeSeries from './Timeseries';
import MyAdvisor from "./MyAdvisor";
//import MyProfile from "./MyProfile";
import Costs from "./portfolio/Costs";
import ProfilesValueChart from './admin/charts/ProfileValueOnTime';
import SignUpForm from "./JoinInvite";


function Nav(props) {
  const logged_out_nav = (
      <span>
        <Link to="/login" onClick={() => props.display_form('login')}>Login</Link>
      </span>
  );

  const role_up_nav = (
      <span>
         <Link to={`/admin/${props.user_id}`} >Dashboard</Link>
          <Link to={`/admin/${props.user_id}/invites`} >Invites</Link>
         <Link to={`/admin/${props.user_id}/users-chart`} >Chart</Link>
      </span>
  );
  const role_down_nav = (
      <span>
         <Link to={`/profile/${props.user_id}`}>Profile</Link>
          <Link to={`/timeseries/${props.user_id}`}>Time Series</Link>
          <Link to={`/my_advisor/${props.user_id}`}>My Advisor</Link>
          {/*<Link to={`/my_profile/${props.user_id}`}>My Profile</Link>*/}
           <Link to={`/portfolio/${props.user_id}`}>Portfolio</Link>
      </span>
  );
  const logged_in_nav = (
      <span>
         {props.role ? role_up_nav : role_down_nav}
      </span>
  );
  return (
        <Router>
            <div className="disp-center">
            <nav>
                <Link to="/" >Home</Link>

                {props.logged_in ? logged_in_nav : logged_out_nav}

                 {props.logged_in
                    ? <Link to="/" onClick={props.handle_logout}>logout</Link>
                    : ''
                }
                <div className="animation start-home"></div>
            </nav>

            </div>
            <div className="main">
                <Route exact path="/profile/:id" component={Profile} />
                 <Route exact path="/timeseries/:id" component={TimeSeries} />
                 <Route exact path="/my_advisor/:id" component={MyAdvisor} />
                  {/*<Route exact path="/my_profile/:id" component={MyProfile} />*/}
                 <Route exact path="/portfolio/:id" component={Costs} />
                 <Route exact path="/admin/:id" component={Home}/>
                 <Route exact path="/admin/:id/invites" component={InviteUsers}/>
                 <Route exact path="/admin/:id/users-chart" component={ProfilesValueChart}/>
                 <Route exact path="/accept/invite/:email/:creator" component={SignUpForm}/>
            </div>
        </Router>
  );
}

export default Nav;

Nav.propTypes = {
  logged_in: PropTypes.bool.isRequired,
  role: PropTypes.bool,
  user_id:PropTypes.number,
  display_form: PropTypes.func.isRequired,
  handle_logout: PropTypes.func.isRequired
};