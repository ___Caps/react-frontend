import React  from 'react';
import './Profile.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChartBar} from "@fortawesome/free-solid-svg-icons";

import { API_URL_PROFILE, API_URL_TOKEN } from "./Static";
import Chart from "./charts/Chart";
import {ErrorModal} from "./ErrorModal";

class Profile extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
        profile: [],
        user_id: -1,
        prev:'',
        next:'',
        modal: false,
        symbol: '',
        error: false,
    };
  }
   openModal = () => this.setState({ error: true })

   closeModal = () => this.setState({ error: false })

    toggle = (el) => {
        this.setState({ modal: !this.state.modal,  symbol : el.symbol});
    };
   close = () => {
        this.setState({ modal: !this.state.modal});
    };

  componentDidMount() {
      this.setState({user_id:this.props.match.params.id})
        const default_url = `${API_URL_PROFILE}/current_user/${this.props.match.params.id}/profile/`
        this.refresh_list(default_url)
  };

  refresh_list(url){
    fetch(url, {
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(data => {
          if(data.count === 0){
            this.openModal();
          }
          this.setState({profile: data.results, next:data.next, prev:data.previous})

    })
  }

  exportData() {
      console.log()
      try {
        fetch(`${API_URL_TOKEN}/data/export/${this.props.match.params.id}/`,{
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }
        })
            .then(response => {
                if ((response.ok === true) && (response.status === 200)) {
                    return response.blob();
                }
            })
            .then(data => {
                const url = window.URL.createObjectURL(data);
                const link = document.createElement("a");
                link.href = url;
                link.setAttribute("download", "portfolio_export.xlsx");
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            });
        } catch (error) {
            console.log(error);
        }
  }

  prevPage = () => {
        let toPage = this.state.prev
        this.refresh_list(toPage);
  }

    nextPage = () => {
        let toPage = this.state.next
        this.refresh_list(toPage);
    }

  render() {
        const { prev, next } = this.state
      return (
          <div>
          {this.state.profile.length
              ? (
                  <div>
                 <div className="pagin">
                     <button className="pg-btn" onClick={this.prevPage}> Prev </button>
                     <button className="pg-btn" onClick={this.nextPage}> Next </button>
                 </div>
          <div className="tbl-center">
           <table className="customers">
               <thead>
            <tr>
                <th>Name</th>
                <th>Symbol</th>
                 <th>Type</th>
                 <th>Region</th>
                 <th>Currency</th>
                <th>Quantity</th>
                <th>TS Chart</th>
            </tr>
            </thead>
               <tbody>
                {this.state.profile.length &&this.state.profile.map(el =>
                    <tr key={el.id}>
                            <td>{el.name}</td>
                           <td>{el.symbol}</td>
                        <td>{el.type}</td>
                        <td>{el.region}</td>
                        <td>{el.currency}</td>
                           <td>{el.quantity}</td>
                           <td style={{textAlign: 'center'}}>
                                <FontAwesomeIcon onClick={() => this.toggle(el)} icon={faChartBar} size="1x" />
                           </td>
                    </tr>)}
               </tbody>
           </table>
         </div>
           <div className="save-portfolio">
                <button className="ts-btn btn btn-primary" onClick={() => this.exportData()} >Download </button>
            </div>

                </div>
              )
            : (<div className="tbl-center" style={{marginTop: 20+"px"}}>Please waiting ...</div>)
          }
          {this.state.modal ? (
              <Chart
                activeItem={this.state.symbol}
                user={this.state.user_id}
                toggle={this.toggle}
                onSave={this.close}
              />
            ) : null}
             {this.state.error &&
             <ErrorModal onOutsideClick={this.closeModal} onEscape={this.closeModal}>
              <h2 style={{color:'#56baed'}}>You do not have any instruments</h2>
              <button ref={button => button && button.focus()} className="button" onClick={this.closeModal}>
                Close Modal
              </button>
             </ErrorModal>
            }
          </div>
      );
  }
}

export default Profile;