import React  from 'react';
import './TimeSeries.css';

import { API_URL_PROFILE } from "./Static";
import { ErrorModal } from "./ErrorModal";


class TimeSeries extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      timeseries: [],

        prev:'',
        next:'',
      user_id: -1,
      showModal: false
    };
  }

  openModal = () => this.setState({ showModal: true })

  closeModal = () => this.setState({ showModal: false })

    prevPage = () => {
        let toPage = this.state.prev
        this.refresh_list(toPage);
    }

    nextPage = () => {
        let toPage = this.state.next
        this.refresh_list(toPage);
    }

  componentDidMount() {
    this.setState({ user_id: this.props.match.params.id })
      const default_url = `${API_URL_PROFILE}/current_user/${this.props.match.params.id}/timeseries/`
    this.refresh_list(default_url)
    };

    refresh_list(url){
        fetch(url, {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }
        })
          .then(res => res.json())
          .then(data => {
              if(data === 409){
                  this.openModal()
                  console.log('limit')
              }
              if(data === 404)
              {
                   this.openModal()
                  console.log('404')
              }
            this.setState({timeseries: data.results, next:data.next, prev:data.previous})
        })
    }


  render() {
        const { prev, next } = this.state
      return (
          <div>
              {this.state.timeseries.length
                  ? (
                      <div>
                          <div className="pagin">
                             <button className="pg-btn" onClick={this.prevPage}> Prev </button>
                            <button className="pg-btn" onClick={this.nextPage}> Next </button>
                         </div>
                      <div className="tbl-center">
                          <table className="timeSeries">
                              <thead>
                              <tr>
                               {this.state.timeseries.length && this.state.timeseries.map(el =>
                                    <th>{el.symbol} </th>
                                )}
                              </tr>
                              </thead>
                              <tbody>
                                  <tr>
                                    {this.state.timeseries.length && this.state.timeseries.map((el, index)=>
                                        <td>
                                         {el.time_series.map((i, index)=>

                                             <div>{i.date } - <b>{i.close_price} <small>{el.currency}</small></b><br/></div>

                                       )}
                                      </td>
                                        )}
                                  </tr>
                              </tbody>
                          </table>

                      </div>
                  </div>
                  )
                  : (<div className="tbl-center" style={{marginTop: 20+"px"}}>Please waiting .....</div>)
              }
            {this.state.showModal &&
             <ErrorModal onOutsideClick={this.closeModal} onEscape={this.closeModal}>
              <h2 style={{color:'#56baed'}}>Something went wrong (limit data provider)</h2>
              <button ref={button => button && button.focus()} className="button" onClick={this.closeModal}>
                Close Modal
              </button>
             </ErrorModal>
            }
          </div>
      );
  }
}

export default TimeSeries;