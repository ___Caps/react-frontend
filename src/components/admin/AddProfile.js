import React, { Component } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Input,
  Label,
} from "reactstrap";
import './EditModal.css'


export default class CustomAddModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
        addItem: this.props.addItem,
        user_id: this.props.user_id
    };
  }

   handleChange = (e) => {
    let { name, value } = e.target;
    const addItem = { ...this.state.addItem, [name]: value };

    this.setState({ addItem });
  };

  render() {
    const { toggle, onSave } = this.props;
    return (
      <Modal isOpen={true} toggle={toggle}>
        <ModalHeader toggle={toggle}>Finance Add Window</ModalHeader>
        <ModalBody>
          <Form>
              <FormGroup>
                  <Label for="u-symbol">Symbol</Label><br/>
                  <Input
                    type="text"
                    id="u-symbol"
                    name="symbol"
                    value={this.state.addItem.symbol}
                    onChange={this.handleChange}
                    placeholder="add symbol"
                  />
            </FormGroup>

            <FormGroup>
              <Label for="u-quantity">Quantity</Label>
              <Input
                type="text"
                id="u-quantity"
                name="quantity"
                value={this.state.addItem.quantity}
                onChange={this.handleChange}
                placeholder="add quality"
              />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button
            className="add-btn"
            onClick={() => onSave(this.state.addItem)}
            >
            Save
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}