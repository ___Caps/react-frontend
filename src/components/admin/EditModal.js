import React, { Component } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Input,
  Label,
} from "reactstrap";
import './EditModal.css'


export default class CustomModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
        activeItem: this.props.activeItem,
        user_id: this.props.user_id
    };
  }



  handleChange = (e) => {
    let { name, value } = e.target;
    const activeItem = { ...this.state.activeItem, [name]: value };

    this.setState({ activeItem });
  };

  render() {
    const { toggle, onSave } = this.props;

    return (
      <Modal isOpen={true} toggle={toggle}>
        <ModalHeader toggle={toggle}>Finance Window</ModalHeader>
        <ModalBody>
          <Form>
            <FormGroup>
              <Label >Symbol :  {this.state.activeItem.symbol}</Label>
            </FormGroup>
            <FormGroup>
              <Label for="u-quantity">Quantity</Label>
              <Input
                type="text"
                id="u-quantity"
                name="quantity"
                value={this.state.activeItem.quality}
                onChange={this.handleChange}
                placeholder="change quality"
              />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button
            className="edit-btn"
            onClick={() => onSave(this.state.activeItem)}
            >
            Save
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}