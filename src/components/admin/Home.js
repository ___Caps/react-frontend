import React from 'react';
import './Home.css';
import {API_URL_BACKEND, API_URL_PROFILE, API_URL_TOKEN} from "../Static";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChartBar, faEdit, faTrashAlt} from "@fortawesome/free-solid-svg-icons";
import Modal from './EditModal';
import AddModal from './AddProfile';
import {ErrorModal} from "../ErrorModal";
import ProfileChart from "./charts/ProfileChart";
import UserChart from "./charts/ManagerChart";

class Home extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
        admin : -1,
        users: [],
        profile: [],
        user_id: -1,
        username: '',
        active: false,
        modal: false,
        show_user_chart: false,
        showModal: false,
        modal_type: false,
        other_err: false,
        update_success: false,
        show_chart: false,
        prev:'',
        next:'',
        activeItem: {
           id : -1,
           symbol: "",
           quantity: -1,
        },
         addItem: {
         id : -1,
         symbol: "",
         quantity: -1,
         owner : -1,
        },
    };
  }
   toggle_user_Chart = (el) => {
        this.setState({ show_user_chart: !this.state.show_user_chart, user_id: el});
    };
    close_user_chart= () => {
        this.setState({ show_user_chart: !this.state.show_user_chart});
    };
   toggle_Chart = () => {
        this.setState({ show_chart: !this.state.show_chart});
    };
   close = () => {
        this.setState({ show_chart: !this.state.show_chart});
    };


    prevPage = () => {
        let toPage = this.state.prev
        this.user_profile_handle(toPage);
    }

    nextPage = () => {
        let toPage = this.state.next
        this.user_profile_handle(toPage);
    }

  openModal = () => this.setState({ showModal: true })

  closeModal = () => this.setState({ showModal: false,
                  other_err:false })

    toggle = () => {
        this.setState({ modal: !this.state.modal });
      };
    toggle_add = () => {
        this.setState({ modal_add: !this.state.modal_add});
      };
    editItem = (item) => {
        this.setState({ activeItem: item , modal: !this.state.modal });
    };
    addItem = () => {
          this.setState({ addItem: '' , modal_add: !this.state.modal_add });
    };

    refreshTimeSeries = () =>
    {
        const url = `${API_URL_PROFILE}/admin/${this.props.match.params.id}/update_timeseries/`;
        fetch(url, {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      })
        .then(res => res.json())
        .then(json => {
            if(json === 1)
            {
                this.setState({update_success: true})
                this.openModal()
            }
            else
            {
                this.setState({ modal_type: true })
                this.openModal()
            }
        });
    }

    componentDidMount() {
       this.refresh_list();
    };

    refresh_list(){
      this.setState({admin: this.props.match.params.id})
      fetch(`${API_URL_BACKEND}/admin/${this.props.match.params.id}/`, {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }
        })
          .then(res => res.json())
          .then(data => {
            this.setState({users:data })

        })
    }

    user_profile_handle(url) {
      fetch(url, {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }
        })
          .then(res => res.json())
          .then(data => {
              if(data.count === 0){
                  let err = [{name:'zero instruments'}];
                  this.setState({ active:true,profile:err})
                  return;
              }
             this.setState({profile:data.results , active:true, next:data.next, prev:data.previous})
        })
    };
    handleDelete = (item) => {
      const currentProfile = this.state.profile;
       fetch(`${API_URL_PROFILE}/admin/${this.state.user_id}/profile/${item.id}/`, {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json',
             Authorization: `JWT ${localStorage.getItem('token')}`
          },
        })
      .then(res =>{
          console.log(res.status)
           if(res.status !== 200)
          {
              this.setState({ other_err: true })
              this.openModal()
              return;
          }
           else{
                this.setState({
                profile: currentProfile.filter(p => p.id !== item.id),
            })
           }
      });
      };


   handleSubmitAdd = async (item) => {
    this.toggle_add();
    item.owner = parseInt(this.state.user_id);
    if (item.symbol) {
        item.quantity = parseInt(item.quantity);
        if (isNaN(item.quantity))
        {
            this.setState({ modal_type: false })
            this.openModal()
            return;
        }
     const requestOptions = {
        method: 'POST',
        headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`,
            'Content-type': 'application/json; charset=UTF-8'
        },
        body: JSON.stringify(item)
    };
    await fetch(`${API_URL_PROFILE}/current_user/add_profile`, requestOptions)
        .then(response => response.json())
        .then(data => {
            if (data === 406) {
                 this.setState({ modal_type: false })
                this.openModal()
            }
            if (data === 226)
            {
                this.setState({ modal_type: true })
                this.openModal()
            }
        })

    }
    await this.user_profile_handle(`${API_URL_PROFILE}/admin/${item.owner}/`)
  };

  handleSubmit = async (item) => {
    this.toggle();

    if (item.id) {
     item.quantity = parseInt(item.quantity);
    if (isNaN(item.quantity))
    {
        this.state.modal_type = false
            this.openModal()
        return;
    }
     const putMethod = {
         method: 'PUT',
         headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`,
             'Content-type': 'application/json; charset=UTF-8'
         },
         body: JSON.stringify(item)
        }
     await fetch(`${API_URL_PROFILE}/current_user/profile/${item.id}`, putMethod)
    .then(response => response.json())
    .then(data => console.log(data)) // Manipulate the data retrieved back, if we want to do something with it
    .catch(err =>  {
        this.state.other_err= true
          this.openModal()
        })
    }
    await this.user_profile_handle(`${API_URL_PROFILE}/admin/${item.owner}/`)
  };

  render() {
   const { prev, next } = this.state
      return (
          <div>
              {this.state.users.length
                  ? (
                      <div className="users-active">
                      <div className="tbl-center">
                          <table className="customers">
                              <thead>
                                  <tr>
                                      <th>Name</th>
                                      <th>Portfolio value</th>
                                       <th>Avatar</th>
                                  </tr>
                              </thead>
                              <tbody>
                              {this.state.users.length && this.state.users.map(el =>
                                  <tr key={el.id}>
                                      <td className="click" onClick={() => {
                                              this.setState({ user_id:  el.id })
                                              this.setState({ username:  el.username })
                                              this.user_profile_handle(`${API_URL_PROFILE}/admin/${el.id}/`)
                                          }} >
                                          {el.username}
                                      </td>

                                      <td onClick={() => {
                                          this.setState({ user_id:  el.id })
                                          this.setState({ username:  el.username })
                                          this.setState({active: false})
                                           this.toggle_user_Chart(el.id)
                                      }} className="click">
                                          Portfolio value for month</td>
                                      <td style={{textAlign: 'center'}}>
                                          <img src={API_URL_TOKEN+el.avatar}
                                            alt="" className="img-icon"/>
                                      </td>
                                  </tr>)}
                              </tbody>
                          </table>
                      </div>
                  <div className="admin-buttons">
                   <button className="ts-btn btn btn-primary" onClick={() => this.refreshTimeSeries()} >Refresh TimeSeries</button>
                   <button className="ts-btn btn btn-primary" onClick={() => {
                                          this.setState({active: false})
                                           this.toggle_Chart()}} >Users portfolio value</button>
                  </div>
               </div>
                  )
                  : (<div className="tbl-center" style={{marginTop: 20+"px"}}>Please waiting .....</div>)
              }
              {this.state.show_chart ? (
              <ProfileChart
                user={this.state.admin}
                toggle={this.toggle_Chart}
                onSave={this.close}
              />
            ) : null}
           {this.state.show_user_chart ? (
          <UserChart
            activeItem={this.state.username}
            user={this.state.user_id}
            toggle={this.toggle_user_Chart}
            onSave={this.close_user_chart}
          />
        ) : null}

              {this.state.active
             ?(
            <div>
                <div className="pages">
                     <h5 className="active-usr">  Active user : {this.state.username}</h5>
                    <div className="pagin">
                         <button className="pg-btn" onClick={this.prevPage}> Prev </button>
                        <button className="pg-btn" onClick={this.nextPage}> Next </button>
                    </div>
                </div>


          <div className="tbl-center">
           <table className="customers">
               <thead>
                <tr>
                    <th>Name</th>
                    <th>Symbol</th>
                     <th>Type</th>
                     <th>Region</th>
                     <th>Currency</th>
                    <th>Quantity</th>
                    <th style={{textAlign: 'center'}}>Edit Delete</th>
                </tr>
               </thead>
               <tbody>
                {this.state.profile.length &&this.state.profile.map(el =>
                    <tr key={el.id}>
                            <td>{el.name}</td>
                           <td>{el.symbol}</td>
                        <td>{el.type}</td>
                        <td>{el.region}</td>
                        <td>{el.currency}</td>
                           <td>{el.quantity}</td>
                           <td style={{textAlign: 'center'}}>
                                <FontAwesomeIcon onClick={() => this.editItem(el)} icon={faEdit} size="1x" />

                                <FontAwesomeIcon style={{marginLeft: 10+'px'}} onClick={() => this.handleDelete(el)} icon={faTrashAlt} size="1x" />
                           </td>
                    </tr>)}
               </tbody>
           </table>
            </div>
            <div className="ok">
                <button className="add-btn btn btn-primary" onClick={() => this.addItem()} >Add Instrument</button>
             </div>
             {this.state.modal_add ? (
              <AddModal
                addItem={this.state.addItem}
                toggle={this.toggle_add}
                onSave={this.handleSubmitAdd}
              />
            ) : null}
        </div>
          )
          : ''
            }

        {this.state.modal ? (
              <Modal
                activeItem={this.state.activeItem}
                toggle={this.toggle}
                onSave={this.handleSubmit}
              />
            ) : null}
         <div style={{marginBottom: 5 + 'em'}}/>
               {this.state.showModal && <ErrorModal onOutsideClick={this.closeModal} onEscape={this.closeModal}>
                   {!this.state.other_err
                   ? (<div>
                       {!this.state.modal_type
                       ? (<h2 style={{color: '#56baed'}}>Enter valid quantity number</h2>)
                       : (<h2 style={{color: '#56baed'}}>This instrument already exist</h2>)
                       }
                       </div>)
                   : (<h2 style={{color: '#56baed'}}>Something went wrong</h2>)
                   }

              <button ref={button => button && button.focus()} className="button" onClick={this.closeModal}>
                Close Modal
              </button>
            </ErrorModal>}

           {this.state.showModal &&
             <ErrorModal onOutsideClick={this.closeModal} onEscape={this.closeModal}>
                 {this.state.update_success
                     ? <h2 style={{color: '#56baed'}}>Timeseries succesfully updated</h2>
                     : (<h2 style={{color: '#56baed'}}>Something went wrong (limit data provider)</h2>)
                 }
              <button ref={button => button && button.focus()} className="button" onClick={this.closeModal}>
                Close Modal
              </button>
             </ErrorModal>
            }
         </div>
      );
  }
}

export default Home;
