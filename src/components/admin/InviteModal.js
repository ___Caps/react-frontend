import React, { Component } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Input,
  Label,
} from "reactstrap";


export default class InviteModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
        addItem: this.props.addItem,
        user_id: this.props.user_id,
        errors: "",
    };
  }

  handleChange = (e) => {
    let { name, value } = e.target;
    const addItem = { ...this.state.addItem, [name]: value };

    this.setState({ addItem });
  };
    validate(){
      let input = this.state.addItem;
      let errors = "";
      let isValid = true;
      if (!input["email"]) {
        isValid = false;
        errors = "Please enter your email Address.";
      }
      if (typeof input["email"] !== "undefined") {
        let pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        if (!pattern.test(input["email"])) {
          isValid = false;
          errors = "Please enter valid email address.";
        }
      }
      this.setState({
        errors: errors
      });
      return isValid;
  }

  render() {
    const { toggle, onSave } = this.props;
    return (
      <Modal isOpen={true} toggle={toggle}>
        <ModalHeader toggle={toggle}>Invite User Window</ModalHeader>
        <ModalBody>
          <Form>
              <FormGroup>
                  <Label for="u-email">Email</Label><br/>
                  <Input
                    type="email"
                    name="email"
                     id="u-email"
                    value={this.state.addItem.email}
                    onChange={this.handleChange}
                    placeholder="Enter email ... "
                  />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
         <Label className="text-danger">{this.state.errors}</Label>
          <Button
            className="add-btn"
            onClick={() => {
                if (this.validate() === true) {
                    onSave(this.state.addItem)
                }
                }
            }
            type="submit"
            >
            Send Invite
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}