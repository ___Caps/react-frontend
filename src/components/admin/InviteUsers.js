import React, { Component } from 'react';
import './Invites.css'
import InviteModal from "./InviteModal";
import {API_URL_TOKEN} from "../Static";
import {ErrorModal} from "../ErrorModal";

class InviteUsers extends Component {
  constructor(props) {
    super(props);
    this.state = {
        modal: false,
        addItem: {
           id : -1,
           email: '',
           admin_id: -1,
        },
        invites: [],
        errorModal: false,
    };
  }

   // add invite modal
    toggle = () => {
        this.setState({ modal: !this.state.modal });
    };

   // email exists modal
    openModal = () => this.setState({ errorModal: true })
    closeModal = () => this.setState({ errorModal: false })

    // submit add invite
   handleSubmit = async (item) => {
    this.toggle();
     item.admin_id=this.props.match.params.id;
     const requestOptions = {
        method: 'POST',
        headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`,
            'Content-type': 'application/json; charset=UTF-8'
        },
        body: JSON.stringify(item)
    };
    await fetch(`${API_URL_TOKEN}/invite/send/`, requestOptions)
        .then(response => response.json())
        .then(data => {
            if (data === 200){
                this.reload_data();
            }
            if(data === 302){
                this.openModal()
            }
        })
  };
   reload_data(){
       fetch(`${API_URL_TOKEN}/invite/list/${this.props.match.params.id}/`, {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }
        })
          .then(res => res.json())
          .then(data => {
            this.setState({invites: data})
        })
   }
  componentDidMount() {
       this.reload_data()
  }

  async resend_invite(email){
       const item = {
           'email':email
       };
       const requestOptions = {
          method: 'PUT',
          headers: {
              Authorization: `JWT ${localStorage.getItem('token')}`,
              'Content-type': 'application/json; charset=UTF-8'
          },
          body: JSON.stringify(item)
      };
      await fetch(`${API_URL_TOKEN}/invite/resend/`, requestOptions)
        .then(response => response.json())
        .then(data => {
            if (data === 200){
                this.reload_data();
            }
            if(data === 302){
                this.openModal()
            }
        })
  }

  render() {
    const { invites } = this.state
    return (
        <div className="center-pos">
          <div className="client-btn">
            <h4><b>Invitation Board</b></h4>
            <button className="btn" onClick={() => {this.toggle()}}>Add client</button>
         </div>
          <div className="invites-tbl">
          <table className="styled-table">
            <thead>
            <tr>
              <th>Email</th>
              <th>Status</th>
            </tr>
            </thead>
            <tbody>
                 {invites.length && invites.map(el => {
                     switch (el.status) {
                         case 'SENT':
                             return ( <tr key={el.id} className="progress-row">
                                  <td>{el.email}</td>
                                  <td>Invitation sent</td>
                                </tr>
                             );
                         case 'ACCEPTED':
                             return ( <tr key={el.id} className="active-row">
                                  <td>{el.email}</td>
                                  <td>Activated</td>
                                </tr>
                             );
                         case "TOKEN_EXPIRED":
                             return (
                                 <tr key={el.id} className="fail-row">
                                   <td>{el.email}</td>
                                   <td>Token Expired
                                        <button className="btn" onClick={() => {this.resend_invite(el.email)}}>Resend</button></td>
                                 </tr>
                             );
                         default:
                             return (
                                 <tr key='1' className="fail-row">
                                   <td>Do not have any invites</td>
                                   <td>Add invite</td>
                                 </tr>
                             );
                     }
                 }
            )}

            </tbody>
          </table>
      </div>
          {this.state.modal ? (
              <InviteModal
                addItem={this.state.addItem}
                toggle={this.toggle}
                onSave={this.handleSubmit}
              />
            ) : null}
             {this.state.errorModal &&
             <ErrorModal onOutsideClick={this.closeModal} onEscape={this.closeModal}>
              <h2 style={{color:'#56baed'}}>Email exists or it's yours</h2>
              <button ref={button => button && button.focus()} className="button" onClick={this.closeModal}>
                Close Modal
              </button>
             </ErrorModal>
            }
        </div>
    );
  }
}

export default InviteUsers;