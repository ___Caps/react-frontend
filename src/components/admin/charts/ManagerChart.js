import React from "react";
import Chart from "react-apexcharts";
import {API_URL_PROFILE} from "../../Static";
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import {Resizable} from "react-timeseries-charts";


class ApexChart extends React.Component {
    constructor(props) {
    super(props);

    this.state = {
      costs: [],
      err:false,
      options: {
        chart: {
          id: "mychart",
          zoom: {
           enabled: true
          },
        },
      yaxis: [{
        "labels": {
            "formatter": function (val) {
                let t = val.toFixed(2)
                return t.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' € '
            }
        }
        }],
      stroke: {
            curve: 'straight'
          },
        xaxis: {
          categories: []
        },
        dataLabels: {
            enabled: false
          },
      grid: {
        row: {
          colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
          opacity: 0.5
        },
      },
      },
      series: [
        {
          name: "series-1",
          data: []
        }
      ],
        activeItem: this.props.username,
        user: this.props.user,
    };
  }
    componentDidMount() {
        this.get_portfolio_values()
    }
    get_portfolio_values(){
        const url = `${API_URL_PROFILE}/current_user/${this.state.user}/daily_prices/`
        fetch(url,{headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }}).then(res => res.json())
          .then(data => {
                if (data === 400){
                    this.setState({err:true})
                    return ;
                }
              const dates = [];
              const values = [];
               Object.entries(data).map(([key, value]) => {
                   dates.push(value['Date'])
                   values.push(value['Sum'])
                })
             const newSeries = [];
            const newXaxis = dates;
            newSeries.push({
              data: values,
              name: 'costs €'
            });
            this.setState({
              series: newSeries,
              options: {
                ...this.state.options,
                labels: newXaxis,
                xaxis: { ...this.state.options.xaxis, categories: newXaxis }
              }
            });
           this.setState({costs: values})
          })
    }
    renderChart() {
    return (
      <div className="app">
        <div className="row">
          <div className="mixed-chart">
            <Chart
              options={this.state.options}
              series={this.state.series}
              type="line"
              width="700"
            />
          </div>
        </div>
      </div>
    );
  }
  render() {
      const {toggle, activeItem} = this.props;
      return (
          <Modal isOpen={true} toggle={toggle} size="lg" style={{maxWidth: '900px', width: '100%', maxHeight: '90%'}}>
              <ModalHeader toggle={toggle}>{activeItem} portfolio value</ModalHeader>
              <ModalBody>
                  <div>
                      {this.state.costs.length
                          ? (
                              <div className="row">
                                  <div className="col-md-12">
                                      <Resizable>{this.renderChart()}</Resizable>
                                  </div>
                              </div>
                          )
                          : (<div>
                              {this.state.err
                              ? (<div>LIMIT DATA PROVIDER</div>)
                              : (<div>Loading ...</div>)
                              }
                          </div>)

                      }
                  </div>
              </ModalBody>
          </Modal>
      );
  }
}
export default ApexChart;