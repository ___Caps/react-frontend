import React from "react";
import Chart from "react-apexcharts";
import {API_URL_PROFILE} from "../../Static";
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import {Resizable} from "react-timeseries-charts";


class ProfileChart extends React.Component {
    constructor(props) {
    super(props);

    this.state = {
      show: false,
      err: false,
      options: {
        chart: {
          id: "mychart",
          zoom: {
           enabled: true
          },
        },
           plotOptions: {
            bar: {
                distributed: true
            }
          },
      colors: ["#F3B415", "#F27036", "#663F59", "#6A6E94", "#4E88B4", "#00A7C6", "#18D8D8", '#A9D794',
            '#46AF78', '#A93F55', '#8C5E58', '#2176FF', '#33A1FD', '#7A918D', '#BAFF29'
          ],
      yaxis: [{
        "labels": {
            "formatter": function (val) {
                let t = val.toFixed(2)
                return t.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' € '
            }
        }
        }],
        xaxis: {
          categories: [],
        },
        dataLabels: {
            enabled: false
          },
      },
      series: [
        {
          name: "series-1",
          data: []
        }
      ],
        activeItem: this.props.username,
        user: this.props.user,
    };
  }
    componentDidMount() {
        this.get_portfolio_values()
    }

    get_portfolio_values(){
        const url = `${API_URL_PROFILE}/admin/${this.state.user}/portfolio_values/`
        fetch(url,{headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }}).then(res => res.json())
          .then(data => {
                if (data === 'API REQUEST LIMIT'){
                    this.setState({err:true})
                    return ;
                }
                const newSeries = [];
                const values = [];
                const newXaxis = [];
                Object.entries(data).map(([key, value]) => {
                    newXaxis.push(key)
                    values.push(value)
                })

            newSeries.push({
              data: values,
              name: '€ '
            });
            this.setState({
              series: newSeries,
              options: {
                ...this.state.options,
                xaxis: { ...this.state.options.xaxis, categories: newXaxis }
              }
            });
            this.setState({show:true})
          })
    }
    renderChart() {
    return (
      <div className="app">
        <div className="row">
          <div className="mixed-chart">
            <Chart
              options={this.state.options}
              series={this.state.series}
              type="bar"
              width="700"
            />
          </div>
        </div>
      </div>
    );
  }
  render() {
      const {toggle} = this.props;
      return (
          <Modal isOpen={true} toggle={toggle} size="lg" style={{maxWidth: '900px', width: '100%', maxHeight: '90%'}}>
              <ModalHeader toggle={toggle}>Users portfolio value</ModalHeader>
              <ModalBody>
                  <div>
                      {this.state.show
                          ? (
                              <div className="row">
                                  <div className="col-md-12">
                                      <Resizable>{this.renderChart()}</Resizable>
                                  </div>
                              </div>
                          )
                          :
                          (<div>
                              {this.state.err
                              ? (<div>LIMIT API CALL DATA PROVIDER</div>)
                              : (<div>Loading ...</div>)
                                }
                          </div>)
                      }
                  </div>
              </ModalBody>
          </Modal>
      );
  }
}
export default ProfileChart;