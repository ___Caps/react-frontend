import React from "react";
import Chart from "react-apexcharts";
import {API_URL_PROFILE} from "../../Static";


class ProfilesValueChart extends React.Component {
    constructor(props) {
    super(props);

     this.state = {
        err:false,
        series: [{
            name: "Session Duration",
            data: [45, 52, 38, 24, 33, 26, 21, 20, 6, 8, 15, 10]
          },
          {
            name: "Page Views",
            data: [35, 41, 62, 42, 13, 18, 29, 37, 36, 51, 32, 35]
          },
          {
            name: 'Total Visits',
            data: [87, 57, 74, 99, 75, 38, 62, 47, 82, 56, 45, 47]
          }],
        options: {
          chart: {
            height: 350,
            type: 'line',
            zoom: {
              enabled: true
            },
          },
        yaxis: [{
            "labels": {
                "formatter": function (val) {
                    if (val !== undefined) {
                        let t = val.toFixed(2)
                        return t.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' € '
                    }
                }
            }
        }],
          dataLabels: {
            enabled: false
          },
          title: {
            text: 'Page Statistics',
            align: 'left'
          },
          legend: {
            tooltipHoverFormatter: function(val, opts) {
              return val + ' - ' + opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] + ''
            }
          },
          markers: {
            size: 0,
            hover: {
              sizeOffset: 6
            }
          },
          xaxis: {
            categories: ['01 Jan', '02 Jan', '03 Jan', '04 Jan', '05 Jan', '06 Jan', '07 Jan', '08 Jan', '09 Jan',
              '10 Jan', '11 Jan', '12 Jan'
            ],
          },
        tooltip: {
            y: [{
                "title": {
                  "formatter": function (val) {
                    return val;
                  }
                }
              },
            ]
          },
          grid: {
            borderColor: '#7f40ff',
          }
        },


      };
    }
    componentDidMount() {
        this.fetch_data()
    }

    fetch_data(){
        const url = `${API_URL_PROFILE}/admin/${this.props.match.params.id}/portfolio_month_values/`
        fetch(url,{headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }}).then(res => res.json())
          .then(data => {
                if (data === 400){
                    this.setState({err:true})
                    return ;
                }
                const newSeries = [];
                const newXaxis = [];
                Object.entries(data).map(([key, value]) => {
                    let list = {name: key, data:  Object.entries(value).map(([key, val]) => {
                        return val['Sum']
                    })}
                    newSeries.push(list)
                });
                Object.entries(data[newSeries[0].name]).map(([k, val]) => {
                        newXaxis.push(val['Date'])
                });
            this.setState({
              series: newSeries,
              options: {
                ...this.state.options,
                xaxis: { ...this.state.options.xaxis, categories: newXaxis }
              }
            });
          })
    }
    renderChart() {
    return (
      <div className="app">
        <div className="row">
          <div className="mixed-chart">
            <Chart
              options={this.state.options}
              series={this.state.series}
              type="line"
              width="700"
            />
          </div>
        </div>
      </div>
    );
  }
  render() {
      return (
          <div>
              {!this.state.err
                  ? (this.renderChart())
                  : (<div>
                      {this.state.err
                          ? (<div style={{display: 'flex', justifyContent: 'center'}}>LIMIT API CALL DATA PROVIDER</div>)
                          : (<div>Loading ...</div>)
                      }
                  </div>)
              }
            </div>
      );
  }
}
export default ProfilesValueChart;