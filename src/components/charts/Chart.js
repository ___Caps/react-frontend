import React from "react";
import moment from "moment";
import './Chart.css';
// Pond
import { Collection, TimeSeries, TimeEvent, TimeRange } from "pondjs";
import {
  Modal,
  ModalHeader,
  ModalBody,
} from "reactstrap";

import {
  Resizable,
  Charts,
  ChartContainer,
  ChartRow,
  YAxis,
  LineChart
} from "react-timeseries-charts";
import {API_URL_PROFILE} from "../Static";


class Chart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            mode: "linear",
            activeItem: this.props.symbol,
            user: this.props.user_id,
            timeseries : [],
            timerange: new TimeRange([1622525264000, Date.now()]),
        };
    }
    handleTimeRangeChange = timerange => {
        this.setState({ timerange });
    };
    componentDidMount() {
        const url = `${API_URL_PROFILE}/current_user/${this.props.user}/timeseries/${this.props.activeItem}/`
        fetch(url,{headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }}).then(res => res.json())
          .then(data => {
              this.setState({timeseries: data[0].time_series});
          })
    }
     make_series() {
         const name = "Instrument-price";
         const columns = ["time", "close",];
         const events = this.state.timeseries.map(item => {
             const timestamp = moment(new Date(item.date));
             const {  close_price,  } = item;
             return new TimeEvent(timestamp.toDate(), {
                 close: +close_price
             });
         });
         const collection = new Collection(events);
         const sortedCollection = collection.sortByTime();
         const series = new TimeSeries({ name, columns, collection: sortedCollection });
         return series;
     }

    renderChart = () => {
       const { timerange } = this.state;
       const series = this.make_series();
        const croppedSeries = series.crop(timerange);
        //const croppedVolumeSeries = seriesVolume.crop(timerange);
        return (
            <div className="chart-container">
            <ChartContainer
                timeRange={timerange}
                hideWeekends={true}
                enablePanZoom={true}
                onTimeRangeChanged={this.handleTimeRangeChange}
                showGrid={true}
                timeAxisStyle={{ axis: { fill: "none", stroke: "none" } }}
            >
                <ChartRow height="350">
                    <Charts>
                        <LineChart
                            axis="y"
                            style={{ close: { normal: { stroke: "red" } } }}
                            columns={["close"]}
                            series={croppedSeries}
                            interpolation="curveBasis"
                        />
                    </Charts>
                    <YAxis
                        style={{
                            label : { fill: "red" },
                            values : {fill: "4f4e4d"},
                            ticks: { fill: "none", stroke: "#4f4e4d" },
                            axis: { fill: "none",  stroke: "#4f4e4d" }
                        }}
                        id="y"
                        label="Price ($)"
                        min={croppedSeries.min("close")}
                        max={croppedSeries.max("close")}
                        format=",.1f"
                        labelOffset={20}
                        width="80"
                        showGrid={true}
                        type={this.state.mode}
                    />
                </ChartRow>
            </ChartContainer>
            </div>
        );
    };

    render() {
    const { toggle, activeItem } = this.props;
        return (
            <Modal isOpen={true} toggle={toggle} size="lg" style={{maxWidth: '900px', width: '100%', maxHeight: '90%'}}>
                <ModalHeader toggle={toggle}>{activeItem} stock price</ModalHeader>
                <ModalBody>
                <div>
                    {this.state.timeseries.length
                    ?(
                    <div className="row">
                        <div className="col-md-12">
                            <Resizable>{this.renderChart()}</Resizable>
                        </div>
                    </div>
                    )
                    : ( <div>Loading ...</div> )
                    }
                </div>
                </ModalBody>
      </Modal>
        );
    }
}

// Export example
export default Chart ;