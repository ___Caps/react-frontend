import React  from 'react';
import {API_URL_PROFILE} from "../Static";
import './Cost.css';
import {ErrorModal} from "../ErrorModal";
import NumberFormat from 'react-number-format';


class Costs extends React.Component {

    constructor(props) {
    super(props);
    this.state = {
      portfolio: [],
      user_id: -1,
      showModal: false
    };
    }

    openModal = () => this.setState({ showModal: true })

    closeModal = () => this.setState({ showModal: false })

    componentDidMount() {
      this.refresh_list()
    };

    refresh_list(){
        const url = `${API_URL_PROFILE}/current_user/${this.props.match.params.id}/daily_prices/`
        fetch(url,{headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }}).then(res => res.json())
          .then(data => {
              if(data !== 400)
              {
                  this.setState({portfolio: data});
              }
              else{
                  this.openModal()
                  console.log('too many requests')
              }
          })
    }

    render() {
      return (
          <div className="main">
          {this.state.portfolio && this.state.portfolio.length
              ? (
                  <div className="tbl-center">
                      <table className="Portfolio">
                          <thead>
                          <th>
                              Last Month
                          </th>
                           <th>
                               Portfolio Costs (euro)
                          </th>
                          </thead>
                          <tbody>
                          {this.state.portfolio.length && this.state.portfolio.map((el, index)=>
                              <tr>
                                    <td>
                                        {el.Date}
                                    </td>
                                    <td>
                                        <NumberFormat style={{color: 'black'}} value={el.Sum} displayType={'text'} thousandSeparator={true} prefix={'€ '} />
                                    </td>
                              </tr>)}
                          </tbody>
                      </table>
                  </div>
              )
            : (<div className="tbl-center" style={{marginTop: 20+"px"}}>Please waiting .....</div>)
          }
          {this.state.showModal &&
             <ErrorModal onOutsideClick={this.closeModal} onEscape={this.closeModal}>
              <h2 style={{color:'#56baed'}}>Something went wrong (limit data provider)</h2>
              <button ref={button => button && button.focus()} className="button" onClick={this.closeModal}>
                Close Modal
              </button>
             </ErrorModal>
            }
          </div>
      );
    }
}

export default Costs;