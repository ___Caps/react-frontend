import { library } from '@fortawesome/fontawesome-svg-core';
import { faEdit, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import {faCalendarAlt} from "@fortawesome/free-regular-svg-icons";

library.add(
  faEdit,
  faTrashAlt,
  faCalendarAlt
  // more icons go here
);